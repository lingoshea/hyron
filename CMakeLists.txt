cmake_minimum_required ( VERSION 3.10 FATAL_ERROR )
project ( hyron )

set ( LIB_SRC
        hyron_basic_obj_t.h         hyron_basic_obj_t.cpp

        obj_rsrc_config_reader.h    obj_rsrc_config_reader.cpp
        quest_config_reader.h       quest_config_reader.cpp
        config_to_objs.h            config_to_objs.cpp
        font_reader.h               font_reader.cpp

        area_map.h                  area_map.cpp
        map_overview.h              map_overview.cpp

        hyron_error.h               hyron_error.cpp
        hyron_phase_command.h       hyron_phase_command.cpp

        export_global_var.h         export_global_var.cpp
        to_bash.h                   to_bash.cpp

        special_fonts.h             special_fonts.cpp
        phase_font.h                phase_font.cpp
        )

add_library ( hyron SHARED ${LIB_SRC} )

set ( HYRON_UNIT_TESTS
        hyron_basic_obj
        config_read
        auto_generate_glocal_var
        phase_font
        )

if ( "${CMAKE_BUILD_TYPE}" MATCHES "Debug" )

    # DEBUG compiler is specifically set to GCC
    set (CMAKE_CXX_FLAGS "-X -fprofile-arcs -fbranch-probabilities -ftest-coverage --coverage -fPIC --pie")

    enable_testing ()
    message("Unit Tests Enabled")

    FOREACH( HYRON_UNIT_TEST_SIG ${HYRON_UNIT_TESTS} )
        add_executable(${HYRON_UNIT_TEST_SIG}
                ${CMAKE_SOURCE_DIR}/tests/${HYRON_UNIT_TEST_SIG}.cpp ${SOURCE_FILES})
        target_link_libraries ( ${HYRON_UNIT_TEST_SIG} hyron )

        target_include_directories( ${HYRON_UNIT_TEST_SIG} PRIVATE ${CMAKE_SOURCE_DIR} )

        add_test( NAME ${HYRON_UNIT_TEST_SIG}
                COMMAND ${HYRON_UNIT_TEST_SIG} )

        message("Unit test `" "${HYRON_UNIT_TEST_SIG}" "` enabled")
    ENDFOREACH( HYRON_UNIT_TEST_SIG )
endif()

add_executable ( hyron_script_sf hyron_script_sf.cpp)
target_link_libraries ( hyron_script_sf hyron )
