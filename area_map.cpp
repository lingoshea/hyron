#include "area_map.h"
#include "hyron_error.h"

objs_in_one_location_t &
cur_location_t::objs_in_a_direction(direction_t direction)
{
    switch (direction)
    {
        case LEFT:
            return left;
        case RIGHT:
            return right;
        case FORWARD:
            return forward;
        case BACKWARD:
            return backward;
        default:
            throw hyron_error(HYRON_ERR_UNKNIWN_DIRECTION);
    }
}
