#ifndef HYRON_AREA_MAP_H
#define HYRON_AREA_MAP_H

#include <vector>

enum direction_t { LEFT, RIGHT, FORWARD, BACKWARD };

class cur_location_t;
class hyron_basic_obj_t;
typedef std::vector < cur_location_t * > objs_in_one_location_t;

// note that hyron DOESNOT any precise location of an object
class cur_location_t {
public:
    explicit cur_location_t (hyron_basic_obj_t & initObj) : obj(initObj) { }

    const hyron_basic_obj_t & obj;

    // surroundings:
    objs_in_one_location_t left;
    objs_in_one_location_t right;
    objs_in_one_location_t forward;
    objs_in_one_location_t backward;

    objs_in_one_location_t &
        objs_in_a_direction(direction_t);
};

#endif //HYRON_AREA_MAP_H
