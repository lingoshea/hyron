#include "config_to_objs.h"

std::vector < hyron_basic_obj_t >
        hyron_config_to_objs ( const obj_rsrc_config_reader& config )
{
    std::vector < hyron_basic_obj_t > list;
    obj_id_t id = 0;

    for (auto objs : config.config_map)
    {
        auto it = objs.second.find ("type");
        if (it != objs.second.end())
        {
            hyron_basic_obj_t obj(id++, *it->second.begin());
        }
        else
        {
            hyron_basic_obj_t obj(id++, "undefined");
        }
    }

    return list;
}
