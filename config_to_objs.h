#ifndef HYRON_CONFIG_TO_OBJS_H
#define HYRON_CONFIG_TO_OBJS_H

#include "hyron_basic_obj_t.h"
#include "obj_rsrc_config_reader.h"

#include <vector>

std::vector < hyron_basic_obj_t > hyron_config_to_objs ( const obj_rsrc_config_reader& );

#endif //HYRON_CONFIG_TO_OBJS_H
