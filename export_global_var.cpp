#include "export_global_var.h"
#include "hyron_error.h"

std::string ltostr(ssize_t l) // less than 256
{
    if ( l >= (9 << 16) )
    {
        throw hyron_error(HYRON_ERR_TOO_MANY_OBJECT);
    }
    char buff [257];
    sprintf(buff, "%ld", l);
    return std::string (buff);
}

/* auto generated vals are
 * static:
 * HYRON_OBJ_TYPE
 * HYRON_OBJ_NAME
 * HYRON_OBJ_ID
 *
 * runtime:
 * HYRON_EVENT
 * HYRON_EVENT_DESC
 * HYRON_FUNC
 *
 * */

std::string export_global_var::func_access_generate(const std::string& func)
{
    std::string vars = "env ";
    auto add_var = [&](const std::string& varname, const std::string& var)
    {
        vars += varname + "=" + '"' + var + "\" ";
    };

    add_var("HYRON_OBJ_NAME", name);
    add_var("HYRON_OBJ_TYPE", type);
    add_var("HYRON_OBJ_ID", ltostr(id) );
    return vars;
}
