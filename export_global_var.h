#ifndef HYRON_EXPORT_GLOBAL_VAR_H
#define HYRON_EXPORT_GLOBAL_VAR_H

#include "obj_rsrc_config_reader.h"
#include "hyron_basic_obj_t.h"

class export_global_var : hyron_basic_obj_t
{
public:

    explicit export_global_var ( obj_id_t InitObj_id, obj_type_t InitObj_type) :
            hyron_basic_obj_t(InitObj_id, InitObj_type) { }

    std::string func_access_generate(const std::string& func);
};


#endif //HYRON_EXPORT_GLOBAL_VAR_H
