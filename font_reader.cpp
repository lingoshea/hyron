#include "font_reader.h"
#include "hyron_error.h"

void font_reader::read_config(std::ifstream & inFile)
{
    file = &inFile;
    try {

        std::string     valname;
        std::string     val;
        std::string     name;
        font_t          font;

        char c;

        try {


            while (inFile) {
                bool expression_begin = false;
                valname.clear();
                val.clear();

                while ((c = next_c()) != '=') {

                    if (is_special_char(c) || is_skippable_char(c)) {
                        if (!expression_begin) {
                            continue;
                        } else {
                            break;
                        }
                    }

                    valname += c;
                    expression_begin = true;
                }

                if (next_c() != '"') {
                    throw hyron_error(HYRON_ERR_INVALID_CONFIG);
                }

                val = read_whole_word();

                if (valname == "font_name") {
                    name = val;
                } else {
                    font[valname] = val;
                }
            }
        }
        catch (hyron_error & e)
        {
            if (e != hyron_error(HYRON_ERR_CONFIG_EOF))
            {
                throw;
            }
        }

        _font_list[name] = font;

        file = nullptr;
    }
    catch (...) {
        throw;
    }
}

bool font_reader::is_skippable_char(char c)
{
    for (const auto & i : skipped_chars)
    {
        if (c == i)
        {
            return true;
        }
    }

    return false;
}
