#ifndef HYRON_FONT_READER_H
#define HYRON_FONT_READER_H

#include "obj_rsrc_config_reader.h"
const char skipped_chars[] = " \n";
typedef std::map < std::string, std::string > font_t;
typedef std::map < std::string, font_t > font_list_t;

class font_reader : private obj_rsrc_config_reader
{
private:

    static bool is_skippable_char(char c);
    font_list_t _font_list;

public:
    font_list_t & font_list = _font_list;
    void read_config(std::ifstream &);
};

#endif //HYRON_FONT_READER_H
