#include "hyron_basic_obj_t.h"
#include "hyron_phase_command.h"
#include "utils.h"

#include <iostream>

const hyron_basic_obj_t *
vector_find_by_name ( objs_in_one_location_t & vec, const std::string & name )
{
    for ( auto & i : vec )
    {
        if ( i->obj.name == name )
        {
            return &i->obj;
        }
    }

    return nullptr;
}

int hyron_basic_obj_t::goto_location(direction_t direction)
{
    auto objs = cur_location.objs_in_a_direction(direction);
    if ( objs.empty() )
    {
        return 0;
    }

    // TODO: script changeable
    std::string output = "There";
    output += ( objs.size() == 1 ? "'s " : "'re " );

    std::cout << output;
    auto it = objs.begin();
    for (; it < objs.end() - 1; it++)
    {
        std::cout << (*it)->obj.name << ", ";
    }
    std::cout << (*it)->obj.name << " here. Which one would you like to interact with?" << std::endl;

    std::string objname, command;

input_name:
    std::getline ( std::cin, objname );
    auto int_obj = vector_find_by_name (objs, objname );
    if ( int_obj == nullptr )
    {
        // TODO: phase_command("unrecognized")?
        std::cout << "object unrecognized" << std::endl;
        goto input_name;
    }

    // TODO: no command?
input_cmd:
    std::getline ( std::cin, command );
    auto ret = phase_command(command);
    if (ret == -1)
    {
        goto input_cmd;
    }

    return ret;
}


