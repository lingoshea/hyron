#ifndef HYRON_HYRON_BASIC_OBJ_T_H
#define HYRON_HYRON_BASIC_OBJ_T_H

#include "area_map.h"
#include "obj_rsrc_config_reader.h"

#include <string>
#include <map>
#include <utility>

// whether it's a tree, stream, house, etc.
// defined in user script, this is just an id
typedef std::string obj_type_t;
typedef std::string event_desc_t; // event description
typedef std::string event_id_t;   // which event is triggered

typedef unsigned long long obj_id_t;

class hyron_basic_obj_t
{
private:
    obj_id_t _id;
    config_list_t _my_config;

public:
    void attach_config(const config_list_t & config)
        { _my_config = config; }

    explicit
    hyron_basic_obj_t(obj_id_t initId, obj_type_t initType)
        : _id(initId), type(std::move(initType)), cur_location(*this) { }

    // public access portal, readonly
    const obj_id_t & id = _id;
    const config_list_t & my_config =  _my_config;

    std::string name;
    std::string desc;  // description
    obj_type_t  type;  // type of the obj, used to trigger event
    cur_location_t cur_location; // current location, surroundings included

    // as an adventure, going towards a place
    // or interacting with objects are the main part
    /* goto_location: return values
     * 0            no objects in given direction
     *
     *
     * */
    int goto_location(direction_t);
};

#endif //HYRON_HYRON_BASIC_OBJ_T_H
