#include "hyron_error.h"

const char *hyron_error::what() const noexcept
{
    if ( ! direct_error_report.empty() )
    {
        return direct_error_report.c_str();
    }

    if ( error_code >= 0 )
    {
        return strerror ( error_code );
    }
    else
    {
        MAP_CAN_FIND_THEN_RETURN_CSTR ( error_code, *user_err_code_map )
        return "undefined error code";
    }
}

bool hyron_error::operator==(const hyron_error & err)
{
    // either of them will be the init value so no point use if-else
    return direct_error_report == err.direct_error_report
        && error_code == err.error_code;
}

