#ifndef HYRON_HYRON_ERROR_H
#define HYRON_HYRON_ERROR_H

#include "utils.h"

#include <exception>
#include <map>
#include <string>
#include <cstring>

#define HYRON_ERR_CONFIG_EOF          "config-EOF"
#define HYRON_ERR_UNKNIWN_DIRECTION   "direction-Unknown"
#define HYRON_ERR_INVALID_CONFIG      "config-Invalid"
#define HYRON_ERR_TOO_MANY_OBJECT     "object-NumOverflow"
#define HYRON_ERR_WRONG_FONT_CONF     "font-WrongCfg"
#define HYRON_ERR_UNEXPECTED          "Unknown-UnexpectedErr"

typedef signed long error_code_t;
typedef std::map < error_code_t, std::string > user_err_code_map_t;

class hyron_error : std::exception {
public:

    // when error code > 0, it's system error code
    // when error code < 0, it's user defined error code
    // error code == 0, successful
    const error_code_t error_code = 0;
    const user_err_code_map_t * user_err_code_map = nullptr;
    const std::string direct_error_report;

    explicit  hyron_error ( signed long _error_code ) :
        error_code(_error_code) { }

    explicit  hyron_error ( signed long _error_code, user_err_code_map_t * _user_err_code_map ) :
            error_code ( _error_code ), user_err_code_map ( _user_err_code_map ) { }

    explicit  hyron_error ( const char * error_report ) :
        direct_error_report ( error_report ) { }

    const char * what() const noexcept override;

    bool operator == (const hyron_error&);
    bool operator != (const  hyron_error& e) { return !this->operator==(e); }
};


#endif //HYRON_HYRON_ERROR_H
