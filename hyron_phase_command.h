#ifndef HYRON_HYRON_PHASE_COMMAND_H
#define HYRON_HYRON_PHASE_COMMAND_H
#include <string>

// phase command basically execute the command within user script
int phase_command(const std::string &);

#endif //HYRON_HYRON_PHASE_COMMAND_H
