#include "obj_rsrc_config_reader.h"
#include "hyron_error.h"

void obj_rsrc_config_reader::load_into_vector()
{
    std::string str_buff;
    char c;
    bool bash_code = false;

    auto suitable_emplace = [&] {
        if (!str_buff.empty())
        {
            _vector_config_list.emplace_back(str_buff);
            if (str_buff == "function")
            {
                bash_code = true;
            }
            str_buff.clear();
        }
    };

    try {
        while (true)
        {
            c = next_c();

            if (bash_code && c == '{')
            {
                _vector_config_list.emplace_back(read_bash_code());
                bash_code = false;
            }

            // empty spaces, ignore
            if (c == ' ')
            {
                suitable_emplace();
                continue;
            }

            // that's a word
            if (c == '"')
            {
                suitable_emplace();
                _vector_config_list.emplace_back(read_whole_word());
                continue;
            }

            // a special char
            if (is_special_char(c))
            {
                suitable_emplace();
                _vector_config_list.emplace_back(1, c);
                continue;
            }

            if (c == '\n')
            {
                suitable_emplace();
                continue;
            }

            str_buff += c;
        }

    }
    catch (...)
    {
        throw;
    }

}

char obj_rsrc_config_reader::next_c()
{
    try {
        if (!file)
        {
            throw hyron_error(HYRON_ERR_UNEXPECTED);
        }

        char c;
        if ( file->get(c) )
        {
            return c;
        }

        file->close();
        throw hyron_error(HYRON_ERR_CONFIG_EOF);
    }
    catch (...)
    {
        throw;
    }
}

std::string obj_rsrc_config_reader::read_whole_word()
{
    std::string buff;
    char c = next_c();
    while (c != '"')
    {
        buff += c;
        c = next_c();
    }

    return buff;
}

bool obj_rsrc_config_reader::is_special_char(char c)
{
    for ( const auto & i : config_special_chars )
    {
        if ( c == i )
        {
            return true;
        }
    }

    return false;
}

void obj_rsrc_config_reader::config_list_generate(std::ifstream & inFile)
{
    file = &inFile;
    _config_map.clear();
    _global_var.clear();

    // TODO: memory usage?
    try {
        try {
            load_into_vector();
        }
        catch (hyron_error &err) {
            if (  not (err == hyron_error(HYRON_ERR_CONFIG_EOF) )  )
            {
                throw;
            }
        }
        catch (...) {
            throw;
        }

        std::string map_name;
        config_list_t * _config_list = nullptr;
        bool inside_obj_domain = false;

        auto auto_domain_detect_n_save =
                [&](const std::string & name,
                const std::vector < std::string > & list) {
            if (inside_obj_domain)
            {
                (*_config_list)[name] = list;
            } else {
                _global_var[name] = list;
            }
        };

        for (auto i = _vector_config_list.begin();
            i < _vector_config_list.end(); i++)
        {
            if (*i == "+")
            {
                _config_list = &_config_map[*++i];
                i++; // {
                inside_obj_domain = true;
            }

            if (*i == "}")
            {
                inside_obj_domain = false;
            }

            if (*i == ":")
            {
                std::vector < std::string > list;
                std::string name = *(i - 1);
                i++;
                while (*i != ";")
                {
                    if (*i != ",") {
                        list.emplace_back(*i);
                    }
                    i++;
                }

                auto_domain_detect_n_save(name, list);
            }

            if (*i == "function")
            {
                std::string name = *(++i);
                std::vector < std::string > code;
                code.emplace_back(*(++i));
                auto_domain_detect_n_save("__FUNC__" + name, code);
            }

            if (*i == "when")
            {
                std::string name = *(++i);
                i++; // ':'
                std::vector < std::string > func;
                func.emplace_back(*(++i));
                auto_domain_detect_n_save("__WHEN__" + name, func);
            }
        }

        _vector_config_list.clear();
        file = nullptr;
    }
    catch (...) {
        throw hyron_error(HYRON_ERR_INVALID_CONFIG);
    }
}

std::string obj_rsrc_config_reader::read_bash_code()
{
    // starting with {
    std::string buff;
    char c;
    int intercap = 0;

    while (true)
    {
        c = next_c();
        if (c == '{')
        {
            intercap++;
        }

        if (c == '}')
        {
            if (intercap == 0)
            {
                break;
            }

            intercap--;
        }
        buff += c;
    }

    return buff;
}
