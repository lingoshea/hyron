#ifndef HYRON_OBJ_RSRC_CONFIG_READER_H
#define HYRON_OBJ_RSRC_CONFIG_READER_H

#include <map>
#include <string>
#include <vector>
#include <fstream>

typedef std::map < std::string, std::vector < std::string > > config_list_t;
typedef std::map < std::string, config_list_t > obj_config_map_t;
const char config_special_chars[] = "+:,{}();";

class obj_rsrc_config_reader {
protected:

    obj_config_map_t _config_map;
    config_list_t    _global_var;

    std::ifstream * file = nullptr;
    std::vector < std::string > _vector_config_list;

    void        load_into_vector();
    std::string read_whole_word();
    static bool        is_special_char(char);
    char        next_c();
    std::string read_bash_code();

public:
    // public access portal, readonly
    const obj_config_map_t & config_map = _config_map;
    const config_list_t &    global_var = _global_var;

    void        config_list_generate(std::ifstream &);
};


#endif //HYRON_OBJ_RSRC_CONFIG_READER_H
