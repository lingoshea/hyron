#include "phase_font.h"
#include "hyron_error.h"

std::string generate_space (long line, long len)
{
    std::string space;
    for (long i = 0; i < line; i++)
    {
        space += std::string( len, ' ' );
        space += "\n";
    }
    space.pop_back();
    return space;
}

long find_char_line_num(const std::string& c)
{
    long line = 0;
    for ( long i = 0; i < c.length() - 2; i++ )
    {
        if (c[i] == '\\' && c[i+1] == '\\' && c[i+2] == 'n') {
            line++;
        }
    }
    return line + 1;
}

std::string next_line (long & offset, const std::string & str)
{
    std::string buff;

    for ( ; offset < str.length() - 1; offset++ )
    {
        if (str[offset] == '\\' && str[offset+1] == 'n') {
            return buff;
        }

        buff += str[offset];
    }

    return buff;
}

std::string combining_2_char(const std::string& char_1,
        const std::string& char_2, int mar)
{
    try {
        std::string char_combined;

        auto line_num_1 = find_char_line_num(char_1);
        auto line_num_2 = find_char_line_num(char_2);

        long off_1 = 0, off_2 = 0;

        if (line_num_1 != line_num_2) {
            throw hyron_error(HYRON_ERR_WRONG_FONT_CONF);
        }

        for (long i = 0; i < line_num_1; i++) {
            std::string
            line_1 = next_line(off_1, char_1),
            line_2 = next_line(off_2, char_2);

            char_combined += line_1.append(mar, ' ') + line_2 + "\\n";
        }

        char_combined.pop_back();
        return char_combined;
    }
    catch (...) {
        throw;
    }
}
