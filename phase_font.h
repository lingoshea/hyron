#ifndef HYRON_PHASE_FONT_H
#define HYRON_PHASE_FONT_H

#include <string>

std::string combining_2_char (const std::string& char_1, const std::string& char_2, int mar = 0);
std::string generate_space   (long, long);
long find_char_line_num      (const std::string& c);

#endif //HYRON_PHASE_FONT_H
