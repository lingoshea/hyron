+tree1
{
  name : "tree1";
  type : "tree";
  interacted : "false";
  location : "0x0", "front";

  when "default" : default;
  when "touched" : touched;
}

+tree2
{
  name : "tree2";
  type : "tree";
  interacted : "false";
  location : "0x0", "backward";

  when "default" : default;
  when "touched" : touched;
}

function touched
{
  echo "oh, that's new. $HYRON_OBJ_NAME says."
}

function default
{
  echo "A beautiful tree in front of you."
}

