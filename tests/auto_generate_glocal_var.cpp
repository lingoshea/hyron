#include "export_global_var.h"
#include "config_to_objs.h"

int main()
{
    std::ifstream file ("../sample/objs.sh");
    obj_rsrc_config_reader configReader;
    configReader.config_list_generate(file);
    auto obj_list = hyron_config_to_objs(configReader);
    return 0;
}
