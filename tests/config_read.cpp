// test config read

#include "obj_rsrc_config_reader.h"

// passed on 7199757fed5bf7bf62cd11fdc17458adf1ef6186
// passed on 72b04f31bf7ea9dff81e86e570a4cfc26c91bf66

int main()
{
    std::ifstream file ("../sample/resources/objs.sh");
    obj_rsrc_config_reader configReader;
    configReader.config_list_generate(file);
    return 0;
}
