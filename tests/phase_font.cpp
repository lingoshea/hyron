#include <iostream>
#include <fstream>
const char COMMAND[] = "interact with the trees";

#include "phase_font.h"
#include "font_reader.h"
#include "utils.h"

int main()
{
    try {
        std::ifstream font("../resources/alligator.font");
        font_reader config;
        config.read_config(font);
        auto font_data = config.font_list.begin()->second;
        std::vector<std::string> converted_str;
        const auto line_num = find_char_line_num(font_data.begin()->second);

        for (const auto &c : COMMAND) {
            if (c == ' ') {
                converted_str.emplace_back(
                        generate_space ( line_num,
                                       strtol(font_data["space_count"].c_str(),
                                              nullptr, 10) )
                                              );
            } else {
                auto it = font_data.find(
                        std::string(1, tolower(c))
                );
                if (it != font_data.end()) {
                    converted_str.emplace_back
                            (font_data[std::string(1, c)]);
                }
            }


        }

        std::string str = converted_str.back();
        converted_str.pop_back();
        while (!converted_str.empty()) {
            str = combining_2_char(converted_str.back(), str);
            converted_str.pop_back();
        }

        system(("echo -e \"" + str + "\"").c_str());

        return 0;
    }
    catch (std::exception & e ) {
        std::cerr << e.what() << std::endl;
        return 1;
    }
}
