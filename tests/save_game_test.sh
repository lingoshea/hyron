#!/usr/bin/env bash

export HYRON_ROOT="$PWD/../utils"
source "../utils/save_game.sh"
source "../utils/load_game.sh"

export __HYRON_SAVE_ENV_HP="100"
export __HYRON_SAVE_ENV_FP="25"
export __HYRON_SAVE_ENV_SP="60"

export DATE
export HYRON_SAVEFILE_PREFIX="/tmp"

function print()
{
  echo "$DATE : $__HYRON_SAVE_ENV_SP"
}

export SAVEFILE="save.001"
save

__HYRON_SAVE_ENV_SP="80"

export SAVEFILE="save.002"
save

export SAVEFILE="save.001"
load
print

export SAVEFILE="save.002"
load
print
