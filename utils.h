#ifndef HYRON_UTILS_H
#define HYRON_UTILS_H

#define CAN_FIND_IN_MAP(e, map)    ((map).find(e) != (map).end())
#define CANNOT_FIND_IN_MAP(e, map) ((map).find(e) == (map).end())

#define MAP_CAN_FIND_THEN_RETURN(elemet, map) { \
    auto _it = (map).find(elemet); \
    if ( _it != (map).end() ) \
    { \
        return _it->second; \
    } \
}

#define MAP_CAN_FIND_THEN_RETURN_CSTR(elemet, map) { \
    auto _it = (map).find(elemet); \
    if ( _it != (map).end() ) \
    { \
        return _it->second.c_str(); \
    } \
}

#endif //HYRON_UTILS_H
