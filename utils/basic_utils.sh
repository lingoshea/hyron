#!/usr/bin/env bash

function get_file_header()
{
  BYTES=$1
  IFS= read -r -d '' -n "$BYTES" __read_header
  RESULT=$?
  echo "$__read_header"
  return $RESULT
}

function getline()
{
  IFS= read -r LINE
  RESULT=$?
  echo "$LINE"
  return $RESULT
}

function hyron_cpy()
{
  cat <&0
}


