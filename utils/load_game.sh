#!/usr/bin/env bash

source "$HYRON_ROOT/basic_utils.sh"

# save game scans all the env vars starting with __HYRON__SAVE__ENV__

function load_date()
{
  FILE="$HYRON_SAVEFILE_PREFIX/$SAVEFILE"
  getline < "$FILE"
}

function load()
{
  data_phase="hyron_cpy"
  FILE="$HYRON_SAVEFILE_PREFIX/$SAVEFILE"
  HYRON_SAVEFILE_MARK="__HYRON_SAVE_ENV_"

  if whereis base64 2>/dev/null >/dev/null && [ $data_phase = "hyron_cpy" ]; then
    data_phase="base64"
  fi

  READ_DATE=false

  while IFS= read -r line
  do
    if ! $READ_DATE; then
      # can be captured if exported before invocation
      DATE="$line"
      READ_DATE=true
      continue
    fi

    export $HYRON_SAVEFILE_MARK"$(echo $line | $data_phase -d)"
  done < "$FILE"
}
