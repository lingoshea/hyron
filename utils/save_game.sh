#!/usr/bin/env bash

source "$HYRON_ROOT/basic_utils.sh"

# save game scans all the env vars starting with __HYRON__SAVE__ENV__
# note that no space is acceptable in variables, replace it with chars

function save()
{
  data_phase="hyron_cpy"
  FILE="$HYRON_SAVEFILE_PREFIX/$SAVEFILE"
  HYRON_SAVEFILE_MARK="__HYRON_SAVE_ENV_"

  if whereis base64 2>/dev/null >/dev/null && [ $data_phase = "hyron_cpy" ]; then
    data_phase="base64"
  fi

  LIST=$(printenv 2>/dev/null | grep "$HYRON_SAVEFILE_MARK" 2>/dev/null)

  date >"$FILE"

  for VAR in $LIST; do
    echo "${VAR/$HYRON_SAVEFILE_MARK/}" | "${data_phase}" >>"$FILE"
  done
}
